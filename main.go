package main

import (
	"crypto/tls"
	"html/template"
	"log"
	"net/http"
	"os"

	"github.com/Unleash/unleash-client-go/v3"
	"github.com/disintegration/imaging"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Main data structure passed into the index.html template.
type PageData struct {
	Hostname               string
	CommitTitle            string
	ProjectPath            string
	PipelineUrl            string
	PipelineIid            string
	ProjectId              string
	ProjectUrl             string
	MergeRequestId         string
	MergeRequestTitle      string
	MergeRequestProjectUrl string
	EnvironmentName        string
	CommitSha              string
	ServerUrl              string
	IsMR                   bool
	ExtendedInfo           bool
	Buttons                []string
}

// Index Return the home page
func Index(w http.ResponseWriter, r *http.Request) {
	var data PageData
	// Fill data structure with environment variables.
	data.CommitTitle = os.Getenv("CI_COMMIT_TITLE")
	data.ProjectPath = os.Getenv("CI_PROJECT_PATH")
	data.ProjectUrl = os.Getenv("CI_PROJECT_URL")
	data.PipelineUrl = os.Getenv("CI_PIPELINE_URL")
	data.PipelineIid = os.Getenv("CI_PIPELINE_IID")
	data.ProjectId = os.Getenv("CI_PROJECT_ID")
	data.MergeRequestId = os.Getenv("CI_MERGE_REQUEST_IID")
	data.MergeRequestTitle = os.Getenv("CI_MERGE_REQUEST_TITLE")
	data.MergeRequestProjectUrl = os.Getenv("CI_MERGE_REQUEST_PROJECT_URL")
	data.EnvironmentName = os.Getenv("CI_ENVIRONMENT_NAME")
	data.CommitSha = os.Getenv("CI_COMMIT_SHA")
	data.ServerUrl = os.Getenv("CI_SERVER_URL")
	data.Hostname = os.Getenv("HOSTNAME")
	data.Buttons = []string{"Huey", "Dewey", "Louie"}
	if len(data.MergeRequestTitle) > 0 {
		data.IsMR = true
	}
	// Get feature flag value.
	data.ExtendedInfo = unleash.IsEnabled("ff-extended-info")
	// Create the index.html by processing a template.
	tmpl := template.Must(template.ParseFiles("static/index.html"))
	tmpl.Execute(w, data)
}

var (
	buttonProcessed = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "ducks_app_button_presses_total",
			Help: "The total number of invocations",
		},
		[]string{"button"},
	)
)

// ButtonsHandler Handle user buttons
func ButtonsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	// Increases the metrics counter.
	buttonProcessed.With(prometheus.Labels{"button": vars["button"]}).Inc()
	log.Println(vars["button"])
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	// Adding a header to the response so the unique pod IP can be displayed in the UI.
	w.Header().Set("Host-Name", os.Getenv("HOSTNAME"))
	w.Header().Set("Cache-Control", "no-cache")
	var filename string = "static/" + vars["button"] + ".jpeg"

	image, err := imaging.Open(filename)
	if err != nil {
		log.Printf("image does not exist: %v", err)
	}
	if image == nil {
		image, _ = imaging.Open("static/404.jpg")
		w.WriteHeader(http.StatusNotFound)
	}
	if unleash.IsEnabled("ff-image-flip") {
		image = imaging.FlipH(image)
	}
	imaging.Encode(w, image, imaging.JPEG)

}

// faviconHandler Handle favicon
func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "static/favicon.ico")
}

func main() {

	// We are ignoring TLS security here for now. SAST scans will report this as a vulnerability.
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	// This initializes Unleash in order to make use of the GitLab feature flag capability.
	unleash.Initialize(
		unleash.WithListener(&unleash.DebugListener{}),
		// This will be the endpoint of the GitLab Unleash server.
		unleash.WithUrl(os.Getenv("CI_SERVER_URL")+"/api/v4/feature_flags/unleash/"+os.Getenv("CI_PROJECT_ID")),
		// The instance id is a GitLab per project setting provided to the application via a Kubernetes secret.
		unleash.WithInstanceId(os.Getenv("INSTANCE_ID")),
		// The environment name is provided to the application via a Kubernetes ConfigMap that gets it's values from the CI job.
		unleash.WithAppName(os.Getenv("CI_ENVIRONMENT_NAME")),
	)

	// Handle request
	r := mux.NewRouter()
	r.HandleFunc("/", Index)
	r.HandleFunc("/favicon.ico", faviconHandler)
	r.HandleFunc("/buttons/{button}", ButtonsHandler)
	http.Handle("/", r)
	// Metrics handler for Prometheus to be able to scrape metrics
	http.Handle("/metrics", promhttp.Handler())

	// serve the app
	log.Printf("Server up on port 5000")
	log.Fatal(http.ListenAndServe(":5000", nil))
}
