module metrics

go 1.14

require (
	github.com/Unleash/unleash-client-go/v3 v3.2.4
	github.com/disintegration/imaging v1.6.2
	github.com/gorilla/mux v1.8.0
	github.com/prometheus/client_golang v1.10.0
)
